/*
SOMMA.Q.DIFF
Aggiunge i quadrati della varianza tra i valori corrispondenti delle due matrici.

Sintassi
SOMMA.Q.DIFF(Matrice X; Matrice Y)

Matrice X rappresenta la prima matrice i cui elementi devono essere sottratti ed elevati al quadrato.

Matrice Y rappresenta la seconda matrice i cui elementi devono essere sottratti ed elevati al quadrato.
*/

package it.unica.pr2.progetto2015.g46834;   

public class Complessa implements it.unica.pr2.progetto2015.interfacce.SheetFunction {

public Complessa(){}

//execute
public Object execute(Object... args){

Double[][] x = (Double[][])args[0];
Double[][] y = (Double[][])args[1];


int j=0,k=0;
double risultato=0;

for(k=0;k<3;k++){
for(j=0;j<2;j++){
risultato+=(Math.abs(x[k][j]-y[k][j])*Math.abs(x[k][j]-y[k][j]));
}
}

return risultato;
}


public final String getCategory(){
return "Matrice";
}

 /** Informazioni di aiuto */
public final String getHelp(){
return "Questa funzione restituisce i quadrati della varianza tra i valori corrispondenti delle due matrici";
}

/*Nome della funzione*/
public final String getName(){
return "SOMMA.Q.DIFF";
}





}




























