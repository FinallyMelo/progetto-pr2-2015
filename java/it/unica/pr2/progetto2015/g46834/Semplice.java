package it.unica.pr2.progetto2015.g46834;   


public class Semplice implements it.unica.pr2.progetto2015.interfacce.SheetFunction {

public Semplice(){}


//Restituisce i caratteri dal primo a "numeroCaratteri", o solo il primo carattere se numeroCaratteri non è passato
public Object execute(Object... args){
 
if(args.length==1){
String stringa=(String) args[0];


return (Object) (stringa.substring(0,1));

}

else{

String stringa=(String) args[0];
int numeroCaratteri= (Integer) args[1];

return (Object) (stringa.substring(0,numeroCaratteri));


}

    }


public final String getCategory(){
return "Matematica";
}

 /** Informazioni di aiuto */
public final String getHelp(){
return "Questa funzione restituisce il primo carattere di una stringa, o i primi n caratteri se chiamata con due parametri";
}

/*Nome della funzione*/
public final String getName(){
return "SINISTRA"; 
}


}
