package it.unica.pr2.progetto2015.g46834;

/*La funzione custom utilizza la libreria HTMLunit.
Prende in input una url e resituisce l'attributo html titolo della pagina.
*/

import java.io.*;
import java.net.URL;
import java.io.IOException;
import java.util.*;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;


public class Custom implements it.unica.pr2.progetto2015.interfacce.SheetFunction {
 

public Custom(){}


@Override
public Object execute(Object... args) { 

String url = (String) args[0];
String titolo=("aa");


try{
        WebClient webClient = new WebClient();
        HtmlPage pagina = webClient.getPage(url);
        titolo= (String) pagina.getTitleText();
         
        }catch(IOException e){
            System.out.println("IOException");
	titolo=("Errore");
        }
               
        return (Object) titolo;
}


@Override
public final String getCategory(){
return "String";
}

@Override
public final String getHelp(){
return "Restituisce il titolo della pagina";
}

@Override
public final String getName(){
return "Custom";
}

}
