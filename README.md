PROGETTO PR2 2015

Autore: Fabrizio Meloni #46834

Funzione Semplice: SINISTRA
Prende in input una stringa ed un intero (numeroCaratteri), oppure solo una stringa. 
Restituisce i caratteri della stringa dal primo a numeroCaratteri; oppure solo il primo carattere della stringa se l'intero non è passato come parametro.

Funzione Complessa: SOMMA.Q.DIFF 
Prende in input due matrici di numeri, e restituisce la somma dei quadrati delle differenze dei valori corrispondenti delle due matrici.


Funzione Custom: GET_PAGE_TITLE
Prende in input una url, e restituisce il titolo della pagina.
